import numpy as np
import sets
repeat = 0
n = 10
rad = 0.1
listarray = range(n)
Coords=([ 0.41717759, 0.42040338], [ 0.31258497, 0.97871788], [ 0.25848859, 0.14562678], [ 0.37904126, 0.03687429], [ 0.76666535, 0.7787851 ], [ 0.94441856, 0.75095194], [ 0.0598637, 0.6544282 ], [ 0.66174842, 0.3077232 ], [ 0.87590891, 0.97417093], [ 0.4307774, 0.53027784])
sprtn=([ True, True, False, True, False, False, False, False, False,  True], [True,  True, True, False, False, False, False, False, False, False], [False, True,  True,  True, False, False, False, False, False, False], [True, False,  True,  True, False, False, False, False, False, False], [False, False, False, False,  True,  True, False, False, False, False], [False, False, False, False,  True,  True, False, False, False, False], [False, False, False, False, False, False,  True, False, False, False], [False, False, False, False, False, False, False,  True, False, False], [False, False, False, False, False, False, False, False,  True, False], [ True, False, False, False, False, False, False, False, False,  True])
Coords = np.array(Coords)
sprtn = np.array(sprtn)
completeflag = False

print listarray
print sprtn


newinput = np.array([0])

def littledr(inarray): ##takes a starting circle and spreads out over the overlapping circles until no more overlaps are found. (Needs to be called multiple times)
	global newinput
	global completeflag
	adjcol = sprtn[:,newinput]
	newinput = np.where(adjcol == True)
	newinput = newinput[0]
	newinput = rmvDup(newinput)
	inarray = rmvDup(inarray)
	if np.array_equal(newinput, inarray):
		completeflag = True
	
	
	
def rmvDup(array): ## Removes all duplicates from an array
	a = sets.Set(array)
	return np.array(list(a))

def findbridge(clump):
	print clump
	cluster1 = {"disks":[], "left":False, "right":False, "top":False, "bottom": False}
	for i in clump:
		if Coords[i,0] < rad:
			cluster1["left"] = True
		elif Coords[i,0] > 1-rad:
			cluster1["right"] = True
		if Coords[i,1] < rad:
			cluster1["bottom"] = True
		elif Coords[i,1] > 1-rad:
			cluster1["top"] = True
	if cluster1["top"] == True and cluster1["bottom"] == True:
		print cluster1
		print "Success!"
	elif cluster1["left"] == True and cluster1["right"] == True:
		print cluster1
		print "Success!"
	else:
		print cluster1
		print "Failure!"



def repeater():
	global completeflag
	global listarray
	global newinput
	while completeflag == False:
		littledr(newinput)
	completeflag = False
	listarray = [x for x in listarray if x not in list(newinput)]
	findbridge(newinput)
	#print listarray
	if len(listarray) > 1:
		newinput = np.array([listarray[0]])
	else:
		print "no solutions"
		return
	repeater()
	
		
	
	
	


repeater()



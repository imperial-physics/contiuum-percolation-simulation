##This code works as is but at very high values of n if there is no bridge it can freeze.

import numpy.random as nr
import numpy as np
import pylab as py
import matplotlib.pyplot as plt
import sets
rad = 0.01 ##Radius
n = 1000 ##Number of disks
completeflag = 0
fullcluster = 0
py.figure(figsize=(5,5)) ##Fixes figure size so circles are circular
sprtn=np.zeros((n,n), dtype="float") ## Empty array to be filled with separation values
Coords = nr.uniform(size = (n, 2)) ##Generates a set of n points in 2D space, these points represent centres of circles
listarray = range(n) ##Generates a list of the indices of the circles. Used later during testing for overlaps

for i in range(n): ##Creates array of separations between circle centres
	tsprtn=np.sqrt((Coords[i,0]-Coords[:,0])**2+(Coords[i,1]-Coords[:,1])**2)
	sprtn[i]= tsprtn



sprtn = sprtn-2*rad ##Subtracts 2*radius from separation. All negative values indicate an overlap
sprtn[sprtn > 0] = 0 ##simplifies separation to 0
sprtn[sprtn < 0] = 1 ##simplifies separation to 1
sprtn = sprtn.astype(bool) ##converts separation array to adjacency array
print sprtn


def littledr(inarray): ##takes a starting circle and spreads out over the overlapping circles until no more overlaps are found. (Needs to be called multiple times)
	global newinput
	global fullcluster
	adjcol = sprtn[:,newinput]
	newinput = np.where(adjcol == True)
	newinput = newinput[0]
	newinput = rmvDup(newinput)
	inarray = rmvDup(inarray)
	if np.array_equal(newinput, inarray):
		fullcluster = 1
	
	
	
def rmvDup(array): ## Removes all duplicates from an array
	a = sets.Set(array)
	return np.array(list(a))
	

def findbridge(clump):
	global completeflag
	cluster = {"disks":[], "left":False, "right":False, "top":False, "bottom": False}
	cluster["disks"] = list(clump)
	for i in clump:
		if Coords[i,0] < rad:
			cluster["left"] = True
		elif Coords[i,0] > 1-rad:
			cluster["right"] = True
		if Coords[i,1] < rad:
			cluster["bottom"] = True
		elif Coords[i,1] > 1-rad:
			cluster["top"] = True
	if cluster["top"] == True and cluster["bottom"] == True:
		print cluster
		print "Success!"
		completeflag = 1
		return 1
	elif cluster["left"] == True and cluster["right"] == True:
		print cluster
		print "Success!"
		completeflag = 1
		return 1
	else:
		print cluster
		print "Failure!"


		
def repeater():
	global listarray
	global newinput
	global completeflag
	global fullcluster
	while fullcluster == 0:
		littledr(newinput)
	listarray = [x for x in listarray if x not in list(newinput)] ## Removes the indices in the found cluster from listarray. Saves time by not rechecking a failed cluster
	if len(newinput) >= 1/(2*rad): ## Only sets findbridge to run if the disks can reach from one side to the other if they were laid end to end (prevents running larger function for hopeless cases)
		if findbridge(newinput) == 1:
			return 1
	else:
		print newinput
	print listarray
	if len(listarray) > 1:
		newinput = np.array([listarray[0]])
	else:
		print "no solutions"
		completeflag = 1
		return 1
	return 0
	
		
	
newinput = np.array([0]) ##Sets the disk 0 as the first disk to be tested	
while completeflag == 0:
	repeater()
	
fig = plt.gcf() ##Plots circles on figure
for i in range(n):
	fig.gca().add_artist(plt.Circle((Coords[i,0],Coords[i,1]), rad, alpha=0.5))
py.show()

import numpy.random as nr
import numpy as np
import pylab as py
import matplotlib.pyplot as plt
rad = 0.1 ##Radius
n = 10 ##Number of disks
##figure(figsize=(4,3)) ##Fixes figure size so circles are circular
py.clf()
srptn=np.zeros((n,n), dtype="float")

Coords = nr.uniform(size = (n, 2))
##print Coords

##Plots circles on figure
fig = plt.gcf()
for i in range(n):
	fig.gca().add_artist(plt.Circle((Coords[i,0],Coords[i,1]), rad, alpha=0.5))
py.show()

for i in range(n):
	tsrptn=np.sqrt((Coords[i,0]-Coords[:,0])**2+(Coords[i,1]-Coords[:,1])**2)
	srptn[i]= tsrptn
	
srptn = srptn-2*rad ##Subtracts 2*radius from seperation. All negative values indicate an overlap
print srptn

cluster={"disks":
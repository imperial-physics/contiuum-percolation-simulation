import numpy.random as nr
import numpy as np
import pylab as py
import matplotlib.pyplot as plt
rad = 0.1 ##Radius
n = 10 ##Number of disks
py.figure(figsize=(5,5)) ##Fixes figure size so circles are circular
py.clf()
sprtn=np.zeros((n,n), dtype="float")

Coords = nr.uniform(size = (n, 2))

##Plots circles on figure
fig = plt.gcf()
for i in range(n):
	fig.gca().add_artist(plt.Circle((Coords[i,0],Coords[i,1]), rad, alpha=0.5))
py.show()
print Coords

for i in range(n):
	tsprtn=np.sqrt((Coords[i,0]-Coords[:,0])**2+(Coords[i,1]-Coords[:,1])**2)
	sprtn[i]= tsprtn
	
sprtn = sprtn-2*rad ##Subtracts 2*radius from separation. All negative values indicate an overlap
sprtn[sprtn > 0] = 0 ##simplifies separation to 0
sprtn[sprtn < 0] = 1 ##simplifies separation to 1
sprtn = sprtn.astype(bool, copy=False) ##converts seperation matrix to adjacency matrix
print sprtn

#for i in range(n):
i=0
cluster1 = {"disks":[], "left":False, "right":False, "top":False, "bottom": False}
if Coords[i,0] < rad:
	cluster1["left"] = True
elif Coords[i,0] > 1-rad:
	cluster1["right"] = True
if Coords[i,1] < rad:
	cluster1["bottom"] = True
elif Coords[i,1] > 1-rad:
	cluster1["top"] = True
print cluster1

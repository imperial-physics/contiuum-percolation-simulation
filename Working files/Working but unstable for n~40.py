import numpy.random as nr
import numpy as np
import pylab as py
import matplotlib.pyplot as plt
import sets
rad = 0.025 ##Radius
n = 50 ##Number of disks
i = 0
py.figure(figsize=(5,5)) ##Fixes figure size so circles are circular
#py.clf()
sprtn=np.zeros((n,n), dtype="float")
Coords = nr.uniform(size = (n, 2))
listarray = range(n)
completeflag = False
print "flag"
##Plots circles on figure
fig = plt.gcf()
for i in range(n):
	fig.gca().add_artist(plt.Circle((Coords[i,0],Coords[i,1]), rad, alpha=0.5))
py.show()
#print Coords
print "flag2"
for i in range(n): ##Creates array of separations between circle centres
	tsprtn=np.sqrt((Coords[i,0]-Coords[:,0])**2+(Coords[i,1]-Coords[:,1])**2)
	sprtn[i]= tsprtn
print "flag3"

sprtn = sprtn-2*rad ##Subtracts 2*radius from separation. All negative values indicate an overlap
sprtn[sprtn > 0] = 0 ##simplifies separation to 0
sprtn[sprtn < 0] = 1 ##simplifies separation to 1
sprtn = sprtn.astype(bool, copy=False) ##converts seperation matrix to adjacency matrix
print sprtn


newinput = np.array([0])

def littledr(inarray): ##takes a starting circle and spreads out over the overlapping circles until no more overlaps are found. (Needs to be called multiple times)
	global newinput
	global completeflag
	adjcol = sprtn[:,newinput]
	newinput = np.where(adjcol == True)
	newinput = newinput[0]
	newinput = rmvDup(newinput)
	inarray = rmvDup(inarray)
	if np.array_equal(newinput, inarray):
		completeflag = True
	
	
	
def rmvDup(array): ## Removes all duplicates from an array
	a = sets.Set(array)
	return np.array(list(a))

def findbridge(clump):
	cluster1 = {"disks":[], "left":False, "right":False, "top":False, "bottom": False}
	cluster1["disks"] = list(clump)
	for i in clump:
		if Coords[i,0] < rad:
			cluster1["left"] = True
		elif Coords[i,0] > 1-rad:
			cluster1["right"] = True
		if Coords[i,1] < rad:
			cluster1["bottom"] = True
		elif Coords[i,1] > 1-rad:
			cluster1["top"] = True
	if cluster1["top"] == True and cluster1["bottom"] == True:
		print cluster1
		print "Success!"
		return 1
	elif cluster1["left"] == True and cluster1["right"] == True:
		print cluster1
		print "Success!"
		return 1
	else:
		print cluster1
		print "Failure!"



def repeater():
	global completeflag
	global listarray
	global newinput
	while completeflag == False:
		littledr(newinput)
	completeflag = False
	listarray = [x for x in listarray if x not in list(newinput)]
	if findbridge(newinput) == 1:
		return
	print listarray
	if len(listarray) > 1:
		newinput = np.array([listarray[0]])
	else:
		print "no solutions"
		return
	repeater()
	
		
	
	
	


repeater()



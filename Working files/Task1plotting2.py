import numpy.random as nr
import numpy
import pylab as py
import matplotlib.pyplot as plt
rad = 0.075 ##Radius
n = 100 ##Number of disks
figure(figsize=(4,3)) ##Fixes figure size so circles are circular


Coords = nr.uniform(size = (n, 2))
print Coords

##Plots circles on figure
fig = plt.gcf()
for i in range(n):
	fig.gca().add_artist(plt.Circle((Coords[i,0],Coords[i,1]), rad, alpha=0.5))